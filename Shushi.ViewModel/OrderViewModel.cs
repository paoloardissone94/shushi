﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Shushi.Model;
using System.Linq;
using Shushi.Services.Abstractions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Shushi.ViewModel
{
    public class OrderViewModel : INotifyPropertyChanged
    {
        private string popupTitle;
        private string restaurantName;
        private List<OrderDish> _orderDishes;
        private bool isRefreshing;
        public Order CurrentOrder { get; set; }
        public Guid Id { get; set; }

       
        public string PopupTitle
        {
            get => popupTitle;
            set
            {
                popupTitle = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PopupTitle)));
            }
        }
        public string RestaurantName
        {
            get { return restaurantName; }
            set
            {
                restaurantName = value;
               
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(RestaurantName)));
                
            }
        }
        public bool IsRefreshing { get => isRefreshing;
            set
            {
                isRefreshing = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsRefreshing)));
            }
        }

        public DateTime Date { get; private set; }
        
        public List<OrderDish> RawOrderDish => _orderDishes;
        public ObservableCollection<OrderTileRow> OrderDishes { get; private set; }
        public ObservableCollection<Dish> Suggestions { get; private set; }

        public IOrderDishService OrderDishService { get; set; }
        public IOrderService OrderService { get; set; }
        public IDishService DishService { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public Command RefreshCommand { get; set; }

        public OrderViewModel(IOrderService orderService, IOrderDishService orderDishService, IDishService dishService)
        {
            _orderDishes = new List<OrderDish>();
            OrderDishes = new ObservableCollection<OrderTileRow>();
            Suggestions = new ObservableCollection<Dish>();

            OrderService = orderService;
            OrderDishService = orderDishService;
            DishService = dishService;
            RefreshCommand = new Command(async () =>
            {
                IsRefreshing = true;
                await ReloadDishes();
                IsRefreshing = false;
            });
        }

        public async Task SetSuggestionsList(string searchText)
        {
            Suggestions.Clear();
            var searchResults = await DishService.GetDishSuggestions(searchText);

            foreach (var item in searchResults)
            {
                Suggestions.Add(item);
            }
        }

        public async Task ReloadDishes()
        {
            _orderDishes = await OrderDishService.GetOrderDishes(CurrentOrder.Id);
            SetOrderDishes();
        }

        public async Task<OrderDish> CreateAndAddDish(string dishName, string dishCode)
        {
            var newDish = await DishService.CreateDish(new Dish { Name = dishName, Code = dishCode });
            return await AddDish(newDish);
        }

        public async Task<OrderDish> AddDish(object dish)
        {
            var toAddDish = dish as Dish;
            OrderDish addedDish;
            var presentDish = _orderDishes.FirstOrDefault(e => e.Dish?.Id == toAddDish.Id);
            if (presentDish is null)
            {
                var newDish = new OrderDish
                {
                    Dish = toAddDish,
                    DishId = toAddDish.Id,
                    OrderId = CurrentOrder.Id,
                    Code = toAddDish.Code,
                    Quantity = 1,
                    DishState = DishState.Ordered
                };
                addedDish = await OrderDishService.CreateOrderDish(newDish);
                _orderDishes.Add(addedDish);
            }
            else
            {
                var current = await OrderDishService.GetOrderDish(presentDish.Id);
                current.Quantity += 1;
                addedDish = await OrderDishService.UpdateOrderDish(current);

                presentDish.Quantity += 1;
            }

            SetOrderDishes();
            return addedDish;
        }

        

        public void SetOrderDishes()
        {
            OrderDishes.Clear();
            var list = new List<OrderTileRow>();
            foreach (var dish in _orderDishes)
            {
                var index = _orderDishes.IndexOf(dish);
                if(index % 2 == 0)
                {
                    list.Add(new OrderTileRow(dish, null));
                }else
                {
                    list.Last().Right = dish;
                }               
            }
            list.ForEach(e => OrderDishes.Add(e));
        }

        public async Task CreateOrder() {
            CurrentOrder = await OrderService.CreateOrder(new Order
            {
                RestaurantName = RestaurantName,
                Date = DateTime.Now
            });
        }

        public async void SetPresentOrder(Guid orderId)
        {
            CurrentOrder = await OrderService.GetOrder(orderId);
            RestaurantName = CurrentOrder.RestaurantName;
        }

        public void ClearOrder()
        {
            OrderDishes.Clear();
        }

        public void ClearSuggestions()
        {
            Suggestions.Clear();
        }

        public async Task SetDishQuantityAndCode(Guid dishId, int quantity, string code)
        {
            var dish = _orderDishes.FirstOrDefault(e => e.Id == dishId);
            
            dish.Quantity = quantity;
            dish.Code = code;
            var resDish = await OrderDishService.UpdateOrderDish(dish);
            dish.Dish = resDish.Dish;

            SetOrderDishes();
        }

        public async Task RemoveDish(Guid id)
        {
            await OrderDishService.DeleteOrderDish(id);
            await ReloadDishes();
        }

        public async Task ChangeDishStatus(Guid id)
        {
            DishState newDishState = DishState.Ordered;
            if (OrderDishes.Any(e => e.Left.Id == id))
            {
                var dish = OrderDishes.First(e => e.Left.Id == id);
                switch (dish.Left.DishState)
                {
                    case (DishState.Ordered):
                        dish.Left.DishState = DishState.Arrived;                       
                        break;
                    case (DishState.Arrived):
                        dish.Left.DishState = DishState.Eaten;
                        break;
                    default:
                        dish.Left.DishState = DishState.Ordered;
                        break;
                }
                newDishState = dish.Left.DishState;
            }
            else if (OrderDishes.Any(e => e.Right.Id == id))
            {
                var dish = OrderDishes.First(e => e.Right.Id == id);
                switch (dish.Right.DishState)
                {
                    case (DishState.Ordered):
                        dish.Right.DishState = DishState.Arrived;
                        break;
                    case (DishState.Arrived):
                        dish.Right.DishState = DishState.Eaten;
                        break;
                    default:
                        dish.Right.DishState = DishState.Ordered;
                        break;
                }
                newDishState = dish.Right.DishState;
            }

            var toUpdate = await OrderDishService.GetOrderDish(id);
            toUpdate.DishState = newDishState;
            await OrderDishService.UpdateOrderDish(toUpdate);
        }

    }
}
