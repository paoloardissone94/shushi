﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using Shushi.Model;
using Shushi.Services.Abstractions;

namespace Shushi.ViewModel
{
    public class DishViewModel : INotifyPropertyChanged
    {
        private string popupTitle;

        private List<Dish> _dishList;

        public IDishService DishService { get; set; }
        public string PopupTitle
        {
            get => popupTitle;
            set
            {
                popupTitle = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PopupTitle)));
            }
        }
        public ObservableCollection<Dish> DishList { get; set; }

        public DishViewModel(IDishService dishService)
        {
            DishService = dishService;
            _dishList = new List<Dish>();
            DishList = new ObservableCollection<Dish>();
        }

        public async Task SetDishList()
        {
            DishList.Clear();
            _dishList = await DishService.GetDishes();

            foreach (var dish in _dishList)
            {
                DishList.Add(dish);
            }
        }

        public void FilterList(string filter)
        {
            DishList.Clear();
            foreach(var dish in _dishList)
            {
                if (dish.Name.Contains(filter))
                {
                    DishList.Add(dish);
                }
            }
        }

        public void ClearFilter()
        {
            DishList.Clear();
            foreach (var dish in _dishList)
            {
                DishList.Add(dish);
            }
        }

        public async Task RemoveDish(Guid id)
        {
            await DishService.DeleteDish(id);
            await SetDishList();
        }

        public async Task CreateNewDish(string name, string code)
        {
            var newDish = new Dish
            {
                Code = code,
                Name = name
            };
            await DishService.CreateDish(newDish);
            await SetDishList();
        }

        public async Task EditDish(Dish toUpdate)
        {          
            await DishService.UpdateDish(toUpdate);
            await SetDishList();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
