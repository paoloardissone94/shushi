﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using Shushi.Model;
using Shushi.Services.Abstractions;

namespace Shushi.ViewModel
{
    public class DashboardViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Order> OrderList { get; set; }
        public IOrderService OrderService { get; }

        public DashboardViewModel(IOrderService orderService)
        {
            OrderList = new ObservableCollection<Order>();
            OrderService = orderService; 
        }

        public async Task SetOrderList()
        {
            OrderList.Clear();
            var orders = await OrderService.GetOrders();

            foreach (var order in orders)
            {
                OrderList.Add(order);
            }
        }

        public async Task RemoveOrder(Guid id)
        {
            await OrderService.DeleteOrder(id);
            await SetOrderList();
        }



        public event PropertyChangedEventHandler PropertyChanged;
    }
}
