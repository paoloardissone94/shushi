﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace Shushi.Model
{
    public class Dish : INotifyPropertyChanged
    {
        private string _code;

        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Code
        {
            get => _code;
            set
            {
                _code = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Code)));
            }
        }

        public string Image { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
       
    }
}
