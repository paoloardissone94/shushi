﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shushi.Model
{
    public class Order
    {
        public Guid Id { get; set; }
        public string RestaurantName { get; set; }
        public DateTime Date { get; set; }
        public List<OrderDish> OrderDishes { get; set; }
    }
}
