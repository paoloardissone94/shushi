﻿using System;
namespace Shushi.Model
{
    public enum DishState
    {
        Ordered=0,
        Arrived=1,
        Eaten=2
    }
}
