﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Shushi.Model
{
    public class OrderDish : INotifyPropertyChanged
    {
        private Dish dish;
        private int quantity;
        private string code;
        private DishState dishState;

        public Guid Id { get; set; }

        public Order Order { get; set; }

        public Guid OrderId { get; set; }
        public Guid DishId { get; set; }

        public Dish Dish
        {
            get => dish;
            set
            {
                dish = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Dish)));
            }
        }

        public int Quantity
        {
            get => quantity;
            set
            {
                quantity = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Quantity)));
            }
        }

        public string Code
        {
            get => code;
            set
            {
                code = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Code)));
            }
        }

        public DishState DishState
        {
            get => dishState;
            set
            {
                dishState = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(DishState)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
