﻿using System;
using System.ComponentModel;

namespace Shushi.Model
{
    public class OrderTileRow : INotifyPropertyChanged
    {
        private OrderDish _left;
        private OrderDish _right;
        
        public OrderDish Left
        {
            get => _left;
            set
            {
                _left = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Left)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HasRightElement)));
            }
        }

        public OrderDish Right
        {
            get => _right;
            set
            {
                _right = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Right)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HasRightElement)));
            }
        }

        public bool HasRightElement
        {
            get 
                {
                var value = !(_right is null);
                return value;
            }
        }

        public OrderTileRow(OrderDish left = null, OrderDish right = null)
        {
            _right = right;
            _left = left;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
