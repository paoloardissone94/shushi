﻿
//namespace Shushi.Droid.Renderers
//{
//    public class AdMobViewRenderer
//    {
//        public AdMobViewRenderer()
//        {
//        }
//    }
//}


using Android.Content;
using Android.Gms.Ads;
using Android.Widget;
using Shushi.Controls;
using Shushi.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(AdControlView), typeof(AdMobViewRenderer))]
namespace Shushi.Droid.Renderers
{
    public class AdMobViewRenderer : ViewRenderer<AdControlView, AdView>
    {
        public AdMobViewRenderer(Context context) : base(context) { }

        private AdView CreateAdView()
        {
            var adView = new AdView(Context)
            {
                AdSize = AdSize.SmartBanner,
                AdUnitId = AppCostants.BannerId,
                LayoutParameters = new LinearLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent)
            };

            adView.LoadAd(new AdRequest.Builder().Build());

            return adView;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<AdControlView> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null && Control == null)
            {
                SetNativeControl(CreateAdView());
            }
        }
    }
}