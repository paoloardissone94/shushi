﻿using System;
using Xamarin.Forms;

namespace Shushi
{
    public static class AppCostants
    {
        public static string AppId
        {
            get
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.Android:
                        return "ca-app-pub-3940256099942544/6300978111";
                    case Device.iOS:
                        return "ca-app-pub-3940256099942544/6300978111";
                    default:
                        return "";
                }
            }
        }

        public static string BannerId
        {
            get
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.Android:
                        return "ca-app-pub-3940256099942544/6300978111";
                    case Device.iOS:
                        return "ca-app-pub-3940256099942544/6300978111";
                    default:
                        return "";
                }
            }
        }
    }
}
