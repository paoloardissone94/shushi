﻿using System;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Shushi.ViewModel;
using Xamarin.Forms;

namespace Shushi.Popups
{
    public partial class CreateOrderPopup : PopupPage
    {
        public string Text { get; set; } = "";
        public CreateOrderPopup(OrderViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = viewModel;
        }

        private async void OnClose(object sender, EventArgs e)
        {
            var bindingContext = BindingContext as OrderViewModel;
            bindingContext.RestaurantName = RestaurantNameEntry.Text;
            await bindingContext.CreateOrder();
            await PopupNavigation.Instance.PopAsync();
        }

        private void OnRestaurantTextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(RestaurantNameEntry.Text))
            {
                okButton.IsEnabled = false;
            }
            else
            {
                okButton.IsEnabled = true;
            }
        }

        protected override bool OnBackgroundClicked()
        {
            return false;
        }
        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return Content.FadeTo(1);
        }
    }
}