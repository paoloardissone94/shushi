﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Shushi.Model;
using Shushi.Services.Abstractions;
using Shushi.ViewModel;
using Xamarin.Forms;

namespace Shushi.Popups
{
    public partial class CreateDishPopup : PopupPage
    {
        public OrderViewModel OrderViewModel { get; set; }
        public DishViewModel DishViewModel { get; set; }
        public Dish ToUpdateDish { get; set; }
        

        public CreateDishPopup(OrderViewModel viewModel)
        {
            InitializeComponent();
            OrderViewModel = viewModel;
            OrderViewModel.PopupTitle = "Create Dish";
            
        }

        public CreateDishPopup(DishViewModel viewModel, Dish toUpdate = null)
        {
            InitializeComponent();
            DishViewModel = viewModel;
            ToUpdateDish = toUpdate;
            DishViewModel.PopupTitle = ToUpdateDish != null ? "Edit Dish" : "Create Dish";
            if(toUpdate != null)
            {
                DishCode.Text = toUpdate.Code;
                DishName.Text = toUpdate.Name;
            }
        }

        private async void OnClose(object sender, EventArgs e)
        {
            if(OrderViewModel is null)
            {
                if (ToUpdateDish != null)
                {
                    ToUpdateDish.Name = DishName.Text;
                    ToUpdateDish.Code = DishCode.Text;
                    await DishViewModel.EditDish(ToUpdateDish);
                }
                else
                {
                    await DishViewModel.CreateNewDish(DishName.Text, DishCode.Text);
                }
            }
            else
            {
                await OrderViewModel.CreateAndAddDish(DishName.Text, DishCode.Text);
            }

            await PopupNavigation.Instance.PopAsync();
        }

        private void EntryTextChanged(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(DishName.Text) || string.IsNullOrWhiteSpace(DishCode.Text))
            {
                okButton.IsEnabled = false;
            }
            else
            {
                okButton.IsEnabled = true;
            }
        }


        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return Content.FadeTo(1);
        }
    }
}