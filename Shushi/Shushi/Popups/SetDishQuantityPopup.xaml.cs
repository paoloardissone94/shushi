﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Shushi.ViewModel;
using Xamarin.Forms;
using System.Linq;
using System.ComponentModel;

namespace Shushi.Popups
{
    public partial class SetDishQuantityPopup : PopupPage
    {
        public Guid DishId { get; set; }
        public int CurrentQuantity { get; set; }
        public string CurrentCode { get; set; }

       
        public SetDishQuantityPopup(OrderViewModel viewModel, Guid dishId) { 
            InitializeComponent();
            BindingContext = viewModel;
            DishId = dishId;
            SetCurrentValues();
        }

        private async void OnClose(object sender, EventArgs e)
        {
            if(!int.TryParse(QuantityEntry.Text, out int quantity))
            {
                quantity = 1;
            }
   
            await (BindingContext as OrderViewModel).SetDishQuantityAndCode(DishId, quantity, CodeEntry.Text);
            await (BindingContext as OrderViewModel).ReloadDishes();
            await PopupNavigation.Instance.PopAsync();
        }

        private void SetCurrentValues()
        {
            var dish = (BindingContext as OrderViewModel).RawOrderDish.FirstOrDefault(e => e.Id == DishId);
            if(dish != null)
            {
                QuantityEntry.Text = dish.Quantity.ToString();
                CodeEntry.Text = dish.Code;
            }
        }

        protected override bool OnBackgroundClicked()
        {
            return false;
        }
        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return Content.FadeTo(1);
        }


    }
}
