﻿using System.Threading.Tasks;
using Shushi.Pages;
using Xamarin.Forms;
using System;
using System.IO;
using Autofac;
using Shushi.Services;
using Shushi.Services.Abstractions;
using Shushi.Store.Data;
using Microsoft.Extensions.DependencyInjection;
using Shushi.Services.Database;

namespace Shushi
{
    sealed partial class App : Application
    {
        public static IContainer ServiceContainer { get; private set; }
        public string DbPath { get; }

        public App(string dbPath)
        {
            InitializeComponent();
            DbPath = dbPath;
            DbContextUtility.CreateDbContext(dbPath);
            ServiceContainer = MainService.BuildServiceContainer(dbPath);
            Initialize();
        }
       
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        private async void Initialize()
        {
            MainPage = new MainPage();
            await Task.Delay(3000);
            MainPage = new NavigationPage(new Dashboard());

        }

       
    }
}
