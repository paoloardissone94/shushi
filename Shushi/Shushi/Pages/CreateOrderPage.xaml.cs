﻿using Rg.Plugins.Popup.Services;
using Shushi.ViewModel;
using Shushi.Popups;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Shushi.Model;
using System;
using System.Linq;
using Autofac;
using Shushi.Services.Abstractions;
using System.Windows.Input;
using System.Threading.Tasks;

namespace Shushi.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateOrderPage : ContentPage
    {
       
        public OrderViewModel ViewModel { get; set; }
        public bool IsRefreshing { get; set; }
        public CreateOrderPage(bool isNewOrder, Guid? orderId = null)
        {
            InitializeComponent();
            ViewModel = new OrderViewModel(App.ServiceContainer.Resolve<IOrderService>(), App.ServiceContainer.Resolve<IOrderDishService>(), App.ServiceContainer.Resolve<IDishService>());
            BindingContext = ViewModel;

            SuggestionsListView.ItemsSource = ViewModel.Suggestions;
            SelectedListView.ItemsSource = ViewModel.OrderDishes;
            SuggestionsListView.IsVisible = false;
            EmptyOrderLayout.IsVisible = false;
            SelectedListView.IsVisible = true;
            SelectedListView.RefreshCommand = new Command(async () =>
            {
                await ViewModel.ReloadDishes();
            });
            if (!isNewOrder)
            {
                ViewModel.SetPresentOrder(orderId.Value);
            }
            
        }

        protected override async void OnAppearing()
        {
            if(ViewModel.CurrentOrder != null)
            {
                await ViewModel.ReloadDishes();
            }
            else
            {
                await PopupNavigation.Instance.PushAsync(new CreateOrderPopup(BindingContext as OrderViewModel));

            }
            CheckEmptyOrder();
            
        }

        async void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if(DishSearchbar.Text != null && DishSearchbar.Text.Length > 2)
            {
                await ViewModel.SetSuggestionsList(DishSearchbar.Text);
            }
            else
            {
                ViewModel.ClearSuggestions();
            }
        }

        void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            SuggestionsListView.IsVisible = true;
            EmptyOrderLayout.IsVisible = false;
            SelectedListView.IsVisible = false;
        }

        //void Handle_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        //{

        //}

        async void ChangeDishStatus(object sender, EventArgs e)
        {
            var button = (Button)sender;
            if(Guid.TryParse(button.CommandParameter.ToString(), out Guid id)){
                await ViewModel.ChangeDishStatus(id);
            }
        }

        async void RemoveDish(object sender, EventArgs e)
        {
            var button = (Button)sender;
            bool answer = await DisplayAlert("Warning", "Delete?", "Yes", "No");
            if (answer)
            {
                if (Guid.TryParse(button.CommandParameter.ToString(), out Guid id))
                {
                    await ViewModel.RemoveDish(id);
                    CheckEmptyOrder();
                }
            }     
        }

        async void EditDish(object sender, EventArgs e)
        {
            var button = (Button)sender;
            if (Guid.TryParse(button.CommandParameter.ToString(), out Guid id))
            {
                try
                {
                    await PopupNavigation.Instance.PushAsync(new SetDishQuantityPopup(BindingContext as OrderViewModel, id));
                }
                catch(Exception)
                {

                }
            }
        }

         async void SuggestionSelected(object sender, EventArgs e)
        {
            var addedDish = await ViewModel.AddDish(SuggestionsListView.SelectedItem);            
            await PopupNavigation.Instance.PushAsync(new SetDishQuantityPopup(BindingContext as OrderViewModel, addedDish.Id));
            DishSearchbar.Unfocus();
            DishSearchbar.Text = string.Empty;
            SuggestionsListView.IsVisible = false;
            SelectedListView.IsVisible = true;
            CheckEmptyOrder();
        }

        private async void CreateDish(object sender, EventArgs e)
        {
            try
            {
                await PopupNavigation.Instance.PushAsync(new CreateDishPopup(BindingContext as OrderViewModel));
                SuggestionsListView.IsVisible = false;
                SelectedListView.IsVisible = true;
                EmptyOrderLayout.IsVisible = false;
            }
            catch (Exception)
            {

            }
        }

        private void CheckEmptyOrder()
        {
            if (ViewModel.OrderDishes.Count == 0)
            {
                SelectedListView.IsVisible = false;
                EmptyOrderLayout.IsVisible = true;
            }
            else
            {
                SelectedListView.IsVisible = true;
                EmptyOrderLayout.IsVisible = false;
            }
        }
    }
}