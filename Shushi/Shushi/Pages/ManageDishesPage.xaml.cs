﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;
using Rg.Plugins.Popup.Services;
using Shushi.Model;
using Shushi.Popups;
using Shushi.Services.Abstractions;
using Shushi.ViewModel;
using Xamarin.Forms;

namespace Shushi.Pages
{
    public partial class ManageDishesPage : ContentPage
    {
        public DishViewModel ViewModel { get; set; }

        public ManageDishesPage()
        {
            InitializeComponent();
            ViewModel = new DishViewModel(App.ServiceContainer.Resolve<IDishService>());
            BindingContext = ViewModel;
        }

        protected override async void OnAppearing()
        {
            await ViewModel.SetDishList();
        }

         void SearchDish(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (DishSearchbar.Text != null && DishSearchbar.Text.Length > 0)
            {
                ViewModel.FilterList(DishSearchbar.Text);
            }
            else
            {
                ViewModel.ClearFilter();
            }
        }

        private async void RemoveDish(object sender, EventArgs e)
        {
            var button = sender as Button;
            bool answer = await DisplayAlert("Warning", "Delete?", "Yes", "No");
            if (answer)
            {
                if (Guid.TryParse(button.CommandParameter.ToString(), out Guid id))
                {
                    await ViewModel.RemoveDish(id);
                }
            }
        }

        private async void EditDish(object sender, EventArgs e)
        {
            try
            {
                var toUpdate = dishList.SelectedItem as Dish;
                await PopupNavigation.Instance.PushAsync(new CreateDishPopup(ViewModel, toUpdate));
            }
            catch (Exception)
            {

            }
        }

        private async void AddNewDish(object sender, EventArgs e)
        {
            try
            {
                await PopupNavigation.Instance.PushAsync(new CreateDishPopup(ViewModel));
            }
            catch (Exception)
            {

            }
        }
    }
}
