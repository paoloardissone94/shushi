﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Shushi.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Shushi;
using Shushi.Services.Abstractions;
using Autofac;
using Shushi.Model;

namespace Shushi.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Dashboard : ContentPage
    {
        public DashboardViewModel ViewModel { get; set; }

        public Dashboard()
        {
            InitializeComponent();
            ViewModel = new DashboardViewModel(App.ServiceContainer.Resolve<IOrderService>());
            BindingContext = ViewModel;
        }

        protected override async void OnAppearing()
        {
           await ViewModel.SetOrderList();
        }

        private async void CreateOrder(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CreateOrderPage(true));
        }

        private async void ManageDishes(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ManageDishesPage());
        }

        private async void OpenOrder(object sender, EventArgs e)
        {
            var orderId = (orderList.SelectedItem as Order).Id;
            await Navigation.PushAsync(new CreateOrderPage(false, orderId));
        }

        private async void RemoveOrder(object sender, EventArgs e)
        {
            var button = (Button)sender;
            bool answer = await DisplayAlert("Warning", "Delete?", "Yes", "No");
            if (answer)
            {
                if (Guid.TryParse(button.CommandParameter.ToString(), out Guid id))
                {
                    await ViewModel.RemoveOrder(id);
                }
            }
        }
    }
}