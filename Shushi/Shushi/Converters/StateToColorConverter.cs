﻿using System;
using System.Globalization;
using Shushi.Model;
using Xamarin.Forms;

namespace Shushi.Converters
{
    public class StateToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
           
            var state = (DishState)value;
            switch (state)
            {
                case DishState.Ordered:
                    return Color.FromHex("2979FF");
                case DishState.Arrived:
                    return Color.FromHex("c7c600");
                case DishState.Eaten:
                    return Color.FromHex("00bb00");
                default:
                    return Color.Red;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
