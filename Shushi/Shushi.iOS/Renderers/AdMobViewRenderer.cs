﻿//using System;
//namespace Shushi.iOS.Renderers
//{
//    public class AdControlViewRenderer
//    {
//        public AdControlViewRenderer()
//        {
//        }
//    }
//}
using System;
using Google.MobileAds;
using Shushi.Controls;
using Shushi.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.Linq;

[assembly: ExportRenderer(typeof(AdControlView), typeof(AdControlViewRenderer))]
namespace Shushi.iOS.Renderers
{
    public class AdControlViewRenderer : ViewRenderer<AdControlView, BannerView>
    {
        private Request GetAdRequest()
        {
            var request = Request.GetDefaultRequest();
            return request;
        }

        private BannerView CreateBannerView()
        {
            var bannerView = new BannerView(AdSizeCons.SmartBannerPortrait)
            {
                AdUnitID = AppCostants.BannerId,
                RootViewController = GetViewController()
            };

            bannerView.LoadRequest(GetAdRequest());

            return bannerView;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<AdControlView> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
            {
                SetNativeControl(CreateBannerView());
            }
        }

        private UIViewController GetViewController()
        {
            var windows = UIApplication.SharedApplication.Windows;
            return (from window in windows where window.RootViewController != null select window.RootViewController).FirstOrDefault();
        }
    }
}
