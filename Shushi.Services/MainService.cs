﻿using System;
using Autofac;
using Shushi.Services.Abstractions;
using Shushi.Services.Database;
using Shushi.Store.Data;

namespace Shushi.Services
{
    public static class MainService
    {
        public static IContainer BuildServiceContainer(string dbPath)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<OrderDishService>().As<IOrderDishService>().SingleInstance();
            builder.RegisterType<DishService>().As<IDishService>().SingleInstance();
            builder.RegisterType<OrderService>().As<IOrderService>().SingleInstance();
            return builder.Build();
        }
    }
}
