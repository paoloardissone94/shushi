﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Shushi.Model;

namespace Shushi.Services.Abstractions
{
    public interface IOrderDishService
    {
        Task<OrderDish> CreateOrderDish(OrderDish orderDish);
        Task<OrderDish> GetOrderDish(Guid orderDishId);
        Task DeleteOrderDish(Guid orderDishId);
        Task<OrderDish> UpdateOrderDish(OrderDish orderDish);
        Task<List<OrderDish>> GetOrderDishes(Guid orderId);
    }
}
