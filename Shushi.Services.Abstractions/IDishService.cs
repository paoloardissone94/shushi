﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Shushi.Model;

namespace Shushi.Services.Abstractions
{
    public interface IDishService
    {
        Task<Dish> CreateDish(Dish dish);
        Task<List<Dish>> GetDishes();
        Task<List<Dish>> GetDishSuggestions(string searchText);
        Task<Dish> GetDish(Guid dishId);
        Task DeleteDish(Guid dishId);
        Task<Dish> UpdateDish(Dish dish);
    }
}
