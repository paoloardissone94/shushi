﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Shushi.Model;

namespace Shushi.Services.Abstractions
{
    public interface IOrderService
    {
        Task<Order> CreateOrder(Order order);
        Task<List<Order>> GetOrders();
        Task<Order> GetOrder(Guid orderId);
        Task DeleteOrder(Guid orderId);
        Task<Order> UpdateOrder(Order order);
    }
}
