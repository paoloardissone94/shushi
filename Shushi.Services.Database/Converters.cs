﻿using System;
using Shushi.Model;
using Shushi.Store.Model;

namespace Shushi.Services.Database
{
    public static class Converters
    {
       public static DishDb ToDbModel(this Dish toConvert)
        {
            return new DishDb
            {
                 Id = toConvert.Id,
                 Code = toConvert.Code,
                 Image = toConvert.Image,
                 Name = toConvert.Name
                 
            };
        }

        public static Dish ToModel(this DishDb toConvert)
        {
            return new Dish
            {
                Id = toConvert.Id,
                Code = toConvert.Code,
                Image = toConvert.Image,
                Name = toConvert.Name
            };
        }

        public static OrderDishDb ToDbModel(this OrderDish toConvert)
        {
            return new OrderDishDb
            {
                Id = toConvert.Id,
                Code = toConvert.Code,
                Quantity = toConvert.Quantity,
                DishState = (Shushi.Store.Model.DishState)toConvert.DishState,
                DishId = toConvert.DishId,
                OrderId = toConvert.OrderId                                
            };
        }

        public static OrderDish ToModel(this OrderDishDb toConvert)
        {
            return new OrderDish
            {
                Id = toConvert.Id,
                Code = toConvert.Code,
                Quantity = toConvert.Quantity,
                DishState = (Shushi.Model.DishState)toConvert.DishState,
                DishId = toConvert.DishId,
                OrderId = toConvert.OrderId,
                Dish = toConvert.Dish?.ToModel(),
                Order = toConvert.Order?.ToModel()
            };
        }

        public static OrderDb ToDbModel(this Order toConvert)
        {
            return new OrderDb
            {
                Id = toConvert.Id,
                Date = toConvert.Date,
                RestaurantName = toConvert.RestaurantName
            };
        }

        public static Order ToModel(this OrderDb toConvert)
        {
            return new Order
            {
                Id = toConvert.Id,
                Date = toConvert.Date,
                RestaurantName = toConvert.RestaurantName
            };
        }
    }
}
