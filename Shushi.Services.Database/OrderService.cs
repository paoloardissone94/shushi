﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Shushi.Model;
using Shushi.Services.Abstractions;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Shushi.Services.Database
{
    public class OrderService : IOrderService
    {
        public async Task<Order> CreateOrder(Order order)
        {
            var context = DbContextUtility.GetDbContext();
            //Da sostituire con converter per passare da app model a store model
            var toAdd = order.ToDbModel();
                var result = await context.Orders.AddAsync(toAdd);
                await context.SaveChangesAsync();

                return result.Entity.ToModel();
            
        }

        public async Task DeleteOrder(Guid orderId)
        {
            var context = DbContextUtility.GetDbContext();
            var toRemove = await context.Orders.FindAsync(orderId);
                if (toRemove != null)
                {
                    context.Orders.Remove(toRemove);
                }
                await context.SaveChangesAsync();
            
        }

        public async Task<Order> GetOrder(Guid orderId)
        {
        var context = DbContextUtility.GetDbContext();
        var result = await context.Orders.FindAsync(orderId);
                if (result != null)
                {
                    return result.ToModel();
                }
                throw new KeyNotFoundException();
            
        }

        public async Task<List<Order>> GetOrders()
        {
            var context = DbContextUtility.GetDbContext();
            var result = await context.Orders.ToListAsync();
                if (result != null)
                {
                    return result.Select(e => e.ToModel()).ToList();
                }
                throw new KeyNotFoundException();
            
        }

        public async Task<Order> UpdateOrder(Order order)
        {
            var context = DbContextUtility.GetDbContext();
            var result = context.Orders.Update(order.ToDbModel());
            await context.SaveChangesAsync();
            return result.Entity.ToModel();
            
        }
    }
}
