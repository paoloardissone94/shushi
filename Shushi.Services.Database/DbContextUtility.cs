﻿using System;
using Shushi.Store.Data;
using Xamarin.Forms;

namespace Shushi.Services.Database
{
    public static class DbContextUtility
    {
        private static ApplicationDbContext DbContext;
       

        public static void CreateDbContext(string dbPath)
        {
           
            DbContext = new ApplicationDbContext(dbPath);
            DbContext.Database.EnsureCreated();
            
        }

        public static ApplicationDbContext GetDbContext()
        {
            return DbContext;
        }
    }
}
