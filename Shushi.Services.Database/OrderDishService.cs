﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Shushi.Model;
using Shushi.Services.Abstractions;
using Shushi.Store.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Shushi.Services.Database
{
    public class OrderDishService : IOrderDishService
    {

        public async Task<OrderDish> CreateOrderDish(OrderDish orderDish)
        {
            var context = DbContextUtility.GetDbContext();
            var ordDish = orderDish.ToDbModel();
            
                var result = await context.OrderDishes.AddAsync(ordDish);
                await context.SaveChangesAsync();

               return result.Entity.ToModel();
            
        }

        public async Task DeleteOrderDish(Guid orderDishId)
        {
            var context = DbContextUtility.GetDbContext();
            var toRemove = await context.OrderDishes.FindAsync(orderDishId);
                if(toRemove != null)
                {
                    context.OrderDishes.Remove(toRemove);
                }
                await context.SaveChangesAsync();
            
        }

        public async Task<OrderDish> GetOrderDish(Guid orderDishId)
        {
            var context = DbContextUtility.GetDbContext();
            var result = await (context.OrderDishes.Include(e => e.Dish)).FirstAsync(e => e.Id == orderDishId);
                if (result != null)
                {
                    return result.ToModel();
                }
                throw new KeyNotFoundException();
            
        }

        public async Task<OrderDish> UpdateOrderDish(OrderDish orderDish)
        {
            var context = DbContextUtility.GetDbContext();
            var toUpdate = await context.OrderDishes.Include(e => e.Dish).FirstAsync(e => e.Id == orderDish.Id);
            toUpdate.Quantity = orderDish.Quantity;
            toUpdate.Code = orderDish.Code;
            toUpdate.DishState = (Store.Model.DishState)orderDish.DishState;
            var result = context.OrderDishes.Update(toUpdate);
            await context.SaveChangesAsync();
            return result.Entity.ToModel();
            
        }

        public async Task<List<OrderDish>> GetOrderDishes(Guid orderId)
        {
            var context = DbContextUtility.GetDbContext();
            var result = await context.OrderDishes
                                    .Where(e => e.OrderId == orderId)
                                    .Include(e => e.Dish)
                                    .Select(e => e.ToModel())
                                    .OrderBy(e => e.Dish.Name)
                                    .ToListAsync();
            if (result != null)
            {
                return result;
            }
            throw new KeyNotFoundException();
        }
    }
}
