﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Shushi.Model;
using Shushi.Services.Abstractions;
using Xamarin.Forms;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Shushi.Services.Database
{
    public class DishService : IDishService
    {
        public async Task<Dish> CreateDish(Dish dish)
        {

            var context = DbContextUtility.GetDbContext();
                //Da sostituire con converter per passare da app model a store model
                var toAdd = dish.ToDbModel();
                var result = await context.Dishes.AddAsync(toAdd);
                await context.SaveChangesAsync();

                return result.Entity.ToModel();
            

        }

        public async Task DeleteDish(Guid dishId)
        {
            var context = DbContextUtility.GetDbContext();
                var toRemove = await context.Dishes.FindAsync(dishId);
                if (toRemove != null)
                {
                    context.Dishes.Remove(toRemove);
                }
                await context.SaveChangesAsync();
            
        }

        public async Task<Dish> GetDish(Guid dishId)
        {
            var context = DbContextUtility.GetDbContext();
            var result = await context.Dishes.FindAsync(dishId);
                if (result != null)
                {
                    return result.ToModel();
                }
                throw new KeyNotFoundException();
            
        }

        public async Task<List<Dish>> GetDishes()
        {
            var context = DbContextUtility.GetDbContext();
            var result = await context.Dishes.Select(e => e.ToModel()).OrderBy(e => e.Name).ToListAsync();

            return result;
        }

        public async Task<List<Dish>> GetDishSuggestions(string searchText)
        {
            var normalizedSearchText = searchText.ToLowerInvariant();
            var context = DbContextUtility.GetDbContext();
            var result = await context.Dishes
                .Where(e => e.Name.ToLowerInvariant().Contains(normalizedSearchText))
                .Select(e => e.ToModel())
                .ToListAsync();
            return result;
        }

        public async Task<Dish> UpdateDish(Dish dish)
        {
            var context = DbContextUtility.GetDbContext();
            var result = context.Dishes.Update(dish.ToDbModel());
            await context.SaveChangesAsync();
            return result.Entity.ToModel();
            
        }
    }
}
