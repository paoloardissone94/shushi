﻿
using Microsoft.EntityFrameworkCore;
using Shushi.Store.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shushi.Store.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<DishDb> Dishes { get; set; }
        public DbSet<OrderDishDb> OrderDishes { get; set; }
        public DbSet<OrderDb> Orders { get; set; }
        public string DbPath { get; }

        public ApplicationDbContext(string dbPath)
        {
            DbPath = dbPath;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source =" + DbPath);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DishDb>().HasKey(e => e.Id);
            modelBuilder.Entity<DishDb>().Property(e => e.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<DishDb>().HasData(
                new { Id = Guid.NewGuid(), Name = "Temaki", Code = "T1" },
                new { Id = Guid.NewGuid(), Name = "Temaki California", Code = "T2" },
                new { Id = Guid.NewGuid(), Name = "Temaki Spicy Tuna", Code = "T3" },
                new { Id = Guid.NewGuid(), Name = "Temaki Tuna", Code = "T4" },
                new { Id = Guid.NewGuid(), Name = "Temaki Philadelphia", Code = "T5" },
                new { Id = Guid.NewGuid(), Name = "Uramaki", Code = "U1" },
                new { Id = Guid.NewGuid(), Name = "Uramaki Tuna", Code = "U2" },
                new { Id = Guid.NewGuid(), Name = "Uramaki Salmon", Code = "U3" },
                new { Id = Guid.NewGuid(), Name = "Uramaki Philadelphia", Code = "U4" },
                new { Id = Guid.NewGuid(), Name = "Uramaki California", Code = "U5" },
                new { Id = Guid.NewGuid(), Name = "Uramaki Tuna Avocado", Code = "U6" },
                new { Id = Guid.NewGuid(), Name = "Uramaki Salmon Avocado", Code = "U6" },
                new { Id = Guid.NewGuid(), Name = "Hossomaki", Code = "H1" },
                new { Id = Guid.NewGuid(), Name = "Hossomaki Philadelphia", Code = "H2" },
                new { Id = Guid.NewGuid(), Name = "Hossomaki Tuna", Code = "H3" },
                new { Id = Guid.NewGuid(), Name = "Hossomaki Avocado", Code = "H4" },
                new { Id = Guid.NewGuid(), Name = "Hossomaki Salmon", Code = "H5" },
                new { Id = Guid.NewGuid(), Name = "Tuna Tartar", Code = "T1" },
                new { Id = Guid.NewGuid(), Name = "Tiger Roll", Code = "R2" },
                new { Id = Guid.NewGuid(), Name = "Salmon Tartar", Code = "T2" },
                new { Id = Guid.NewGuid(), Name = "Onigiri", Code = "O1" },
                new { Id = Guid.NewGuid(), Name = "Onigiri Spicy Tuna", Code = "O2" },
                new { Id = Guid.NewGuid(), Name = "Onigiri Tuna", Code = "O3" },
                new { Id = Guid.NewGuid(), Name = "Onigiri Ebiten", Code = "O4" },
                new { Id = Guid.NewGuid(), Name = "Nighiri", Code = "N1" },
                new { Id = Guid.NewGuid(), Name = "Nighiri Salmon", Code = "N2" },
                new { Id = Guid.NewGuid(), Name = "Nighiri Tuna", Code = "N3" },
                new { Id = Guid.NewGuid(), Name = "Nighiri Avocado", Code = "N4" },
                new { Id = Guid.NewGuid(), Name = "Gunkan", Code = "G1" },
                new { Id = Guid.NewGuid(), Name = "Futomaki", Code = "F1" },
                new { Id = Guid.NewGuid(), Name = "Futomaki California", Code = "F2" },
                new { Id = Guid.NewGuid(), Name = "Chirashi Salmon", Code = "C1" },
                new { Id = Guid.NewGuid(), Name = "Chirashi Tuna", Code = "C2" },
                new { Id = Guid.NewGuid(), Name = "Chirashi Mix", Code = "C3" },
                new { Id = Guid.NewGuid(), Name = "Miso Soup", Code = "S1" },
                new { Id = Guid.NewGuid(), Name = "Ramen", Code = "S2" },
                new { Id = Guid.NewGuid(), Name = "Soy Spaghetti", Code = "S3" }
                );
            
            modelBuilder.Entity<OrderDishDb>().HasKey(e => e.Id);
            modelBuilder.Entity<OrderDishDb>().Property(e => e.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<OrderDishDb>().HasOne(e => e.Dish).WithMany(e => e.OrderDishes).HasForeignKey(e => e.DishId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<OrderDishDb>().HasOne(e => e.Order).WithMany(e => e.OrderDishes).HasForeignKey(e => e.OrderId).OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<OrderDb>().HasKey(e => e.Id);
            modelBuilder.Entity<OrderDb>().Property(e => e.Id).ValueGeneratedOnAdd();
            //Non sono sicuro che sia necessario. Bisognare provare
            base.OnModelCreating(modelBuilder);
        }
    }
}
