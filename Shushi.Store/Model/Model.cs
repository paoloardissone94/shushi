﻿using System;
using System.Collections.Generic;

namespace Shushi.Store.Model
{

    public class OrderDb
    {
        public Guid Id { get; set; }
        public string RestaurantName { get; set; }
        public DateTime Date { get; set; }
        public List<OrderDishDb> OrderDishes { get; set; } = new List<OrderDishDb>();
    }

    public class OrderDishDb
    {
        public Guid Id { get; set; }
        public int Quantity { get; set; }
        public string Code { get; set; }
        public DishState DishState { get; set; }

        public Guid OrderId { get; set; }
        public Guid DishId { get; set; }
        public DishDb Dish { get; set; }
        public OrderDb Order { get; set; }
    }
    public class DishDb
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Image { get; set; }

    public List<OrderDishDb> OrderDishes { get; set; } = new List<OrderDishDb>();
    }

    public enum DishState
    {
        Ordered = 0,
        Arrived = 1,
        Eaten = 2
    }
}
